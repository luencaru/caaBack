from django.contrib import admin

# Register your models here.
from apps.products.models import Category, Brand, Family, Product

admin.site.register(Category)
admin.site.register(Brand)
admin.site.register(Family)
admin.site.register(Product)
