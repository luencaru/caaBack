from rest_framework.generics import ListAPIView, get_object_or_404

from .models import Category, Brand, Family
from .serializers import CategorySerializer, BrandSerializer, FamilySerializer, ProductSerializer


class ListCategoryAPI(ListAPIView):
    serializer_class = CategorySerializer
    queryset = Category.objects.all().order_by('name')


class ListBrandAPI(ListAPIView):
    serializer_class = BrandSerializer

    def get_queryset(self):
        categoria = get_object_or_404(Category.objects.all(), id=self.kwargs['pk'])
        return categoria.brands.all().order_by('name')


class ListFamilyAPI(ListAPIView):
    serializer_class = FamilySerializer

    def get_queryset(self):
        brand = get_object_or_404(Brand.objects.all(), id=self.kwargs['pk'])
        return brand.families.all().order_by('name')


class ListProductAPI(ListAPIView):
    serializer_class = ProductSerializer

    def get_queryset(self):
        family = get_object_or_404(Family.objects.all(), id=self.kwargs['pk'])
        return family.products.all().order_by('name')
