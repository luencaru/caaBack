from apps.products.models import Product, Category, Brand, Family

import os
from django.core.management.base import BaseCommand

import json


class Command(BaseCommand):
    def handle(self, *args, **options):
        with open(os.path.join(os.path.dirname(os.path.abspath(__file__)), "data/products.json")) as f:
            reader = json.load(f)
            list_products = list(reader)

            for product_json in list_products:
                print(product_json)
                category, bc = Category.objects.get_or_create(name=product_json.get('DE_CATE', "NN"))
                brand, bb = Brand.objects.get_or_create(name=product_json.get('DE_EQUI', "NN"))
                brand.category.add(category)
                family, bf = Family.objects.get_or_create(name=product_json.get('DE_FAMI', "NN"), brand=brand)
                product, bp = Product.objects.get_or_create(name=product_json.get('DE_ITEM', "NN"),
                                                            id=product_json.get('ID_ITEM', "NN"),
                                                            family=family)