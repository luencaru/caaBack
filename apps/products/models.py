from django.db import models


# Create your models here.
class Category(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    last_modified = models.DateTimeField(auto_now=True)
    name = models.CharField(max_length=500, unique=True, verbose_name='Nombre')

    def __str__(self):
        return '{} - {}'.format(self.id, self.name)


class Brand(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    last_modified = models.DateTimeField(auto_now=True)
    name = models.CharField(max_length=500, unique=True, verbose_name='Nombre')
    category = models.ManyToManyField(Category, related_name='brands', verbose_name='Categoría')

    def __str__(self):
        return '{} - {}'.format(self.id, self.name)


class Family(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    last_modified = models.DateTimeField(auto_now=True)
    name = models.CharField(max_length=500, verbose_name='Nombre')
    brand = models.ForeignKey(Brand, related_name='families', null=True, blank=True, verbose_name='Marca')

    def __str__(self):
        return '{} - {}'.format(self.id, self.name)


class Product(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    last_modified = models.DateTimeField(auto_now=True)
    name = models.CharField(max_length=500, verbose_name='Nombre')
    family = models.ForeignKey(Family, related_name='products', null=True, blank=True, verbose_name='Familia')

    def __str__(self):
        return '{} - {}'.format(self.id, self.name)
