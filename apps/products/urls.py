from django.conf.urls import url

from .views import ListCategoryAPI, ListBrandAPI, ListFamilyAPI, ListProductAPI

urlpatterns = [

    url(r'^categories/$', ListCategoryAPI.as_view()),
    url(r'^categories/(?P<pk>\d+)/brands/$', ListBrandAPI.as_view()),
    url(r'^categories/brands/(?P<pk>\d+)/families/$', ListFamilyAPI.as_view()),
    url(r'^categories/brands/families/(?P<pk>\d+)/products/$', ListProductAPI.as_view()),

]
